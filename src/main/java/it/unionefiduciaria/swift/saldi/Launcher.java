package it.unionefiduciaria.swift.saldi;

import java.io.IOException;

import it.unionefiduciaria.swift.saldi.adapters.shellAdapter.ShellMode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Launcher {


    public static final Logger logger = LogManager.getLogger(Launcher.class);


    public static void main(String[] args) throws IOException {

        SpringApplication.run(Launcher.class, args);


        logger.debug("Eseguito");

    }

}
