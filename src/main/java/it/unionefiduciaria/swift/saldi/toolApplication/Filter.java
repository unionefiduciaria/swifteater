package it.unionefiduciaria.swift.saldi.toolApplication;

import com.prowidesoftware.swift.model.SwiftMessage;

import java.util.List;

public class Filter {


    /*TODO:ridefinire il filtro*/

    private static SwiftMessage swiftMessage;
    private static SwiftMessage returnSwiftMessage;
    private static Filter filter;


    public static Filter filter(SwiftMessage swiftMessage) {

        filter.swiftMessage = swiftMessage;

        return filter;
    }


    //    exclude this list of sender
    public static Filter notIfSender(List<String> sender) {


        if (!filter.swiftMessage.getSender().equals(sender)) {

            returnSwiftMessage = null;

        } else {

            returnSwiftMessage = filter.swiftMessage;
        }

        return filter;
    }


    //    exclude this sender
    public static Filter notIfSender(String sender) {


        if (!sender.contains(filter.swiftMessage.getSender())) {

            returnSwiftMessage = null;

        } else {

            returnSwiftMessage = filter.swiftMessage;
        }

        return filter;
    }


    //    select this list of type
    public static Filter ifType(List<String> type) {


        if (type.contains(filter.swiftMessage.getType())) {

            returnSwiftMessage = filter.swiftMessage;

        } else {

            returnSwiftMessage = null;

        }

        return filter;
    }


    //    select this list of type
    public static Filter ifType(String type) {


        if (filter.swiftMessage.getType().equals(type)) {

            returnSwiftMessage = filter.swiftMessage;

        } else {

            returnSwiftMessage = null;

        }

        return filter;
    }


    //    return
    public static SwiftMessage toSwiftMessage() {

        return returnSwiftMessage;
    }

}
