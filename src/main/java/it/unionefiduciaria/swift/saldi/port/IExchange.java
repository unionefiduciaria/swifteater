package it.unionefiduciaria.swift.saldi.port;

import it.unionefiduciaria.swift.saldi.model.ConvertedAmount;

public interface IExchange {

    public ConvertedAmount getConvertAmount(String currencyFrom, String currencyTo, String amount, String date);

}
