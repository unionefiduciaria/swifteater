package it.unionefiduciaria.swift.saldi.port;

import it.unionefiduciaria.swift.saldi.model.ConvertedAmount;

public interface IRestExchangePort {

    /*TODO: implementare un metodo mappato sulle rischiesta al micro servizio,
         che chiama  un'alto metodo non mappato che restituisce
         il tasso di cambio recuperato da un'altro micro servizio */


    public ConvertedAmount restModel(String divConto, String saldo, String divisa, String date);

}
