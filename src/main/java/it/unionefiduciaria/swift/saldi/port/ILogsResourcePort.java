package it.unionefiduciaria.swift.saldi.port;

import com.prowidesoftware.swift.model.SwiftMessage;

public interface ILogsResourcePort {

    public void addLog(SwiftMessage swiftMessage, String nameDir);

}
