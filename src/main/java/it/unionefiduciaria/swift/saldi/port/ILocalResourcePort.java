package it.unionefiduciaria.swift.saldi.port;

import com.prowidesoftware.swift.model.SwiftMessage;

import java.io.IOException;
import java.util.List;

public interface ILocalResourcePort {


    List<SwiftMessage> readLocalResource(String directory) throws IOException;

    void saveLocalSwiftMessage(List<SwiftMessage> listSwiftMessage) throws IOException;
}
