package it.unionefiduciaria.swift.saldi.port;

import com.prowidesoftware.swift.model.SwiftMessage;

import java.io.IOException;
import java.util.List;

public interface ISwiftMessageManager {

    List<SwiftMessage> readResource(String dir) throws IOException;

    void StoreFile(List<SwiftMessage> swift) throws IOException;

    void saveLog(List<SwiftMessage> swift, String nameDir);

}