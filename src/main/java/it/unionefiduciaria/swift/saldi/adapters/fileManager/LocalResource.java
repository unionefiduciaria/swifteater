package it.unionefiduciaria.swift.saldi.adapters.fileManager;

import com.prowidesoftware.swift.io.writer.SwiftWriter;
import com.prowidesoftware.swift.model.SwiftMessage;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import com.prowidesoftware.swift.utils.Lib;
import it.unionefiduciaria.swift.saldi.port.ILocalResourcePort;
import org.springframework.stereotype.Component;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

//  classe utilizzata per gestire fonte dati da file
public class LocalResource implements ILocalResourcePort {


    //    leggo il contenuto del file
    @Override
    public List<SwiftMessage> readLocalResource(String directory) throws IOException {

        List<SwiftMessage> listSwiftMessage = new ArrayList<>();

        AbstractMT abstractMT = AbstractMT.parse(Lib.readResource(directory));
        SwiftMessage swiftMessageObject = abstractMT.getSwiftMessage();

        for (int i = 0; i < swiftMessageObject.getUnparsedTextsSize(); i++) {

            SwiftMessage swiftMessage = swiftMessageObject.unparsedTextGetAsMessage(i);
            listSwiftMessage.add(swiftMessage);

        }
        return listSwiftMessage;
    }


    //     salvo una lista di messaggi un nuovo file per ogni messaggio swift
    @Override
    public void saveLocalSwiftMessage(List<SwiftMessage> listSwiftMessage) throws IOException {

        for (SwiftMessage swiftMessage : listSwiftMessage) {

            new File("src\\main\\Output\\" + LocalDate.now() + "\\" + swiftMessage.getSender()).mkdirs();

            FileWriter fileWriter = new FileWriter("src\\main\\Output\\" + LocalDate.now() + "\\" + swiftMessage.getSender() + "\\"

                    + LocalDate.now() + swiftMessage.getUUID() + ".txt");

            BufferedWriter out = new BufferedWriter(fileWriter);

            SwiftWriter.writeMessage(swiftMessage, out);
            out.close();

        }

//        logger.info("Store file eseguito");

    }


}
