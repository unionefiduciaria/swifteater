package it.unionefiduciaria.swift.saldi.adapters.logsManager;


import lombok.Data;

@Data
public class SaldoSwift {


    private String bic;
    private String messaggio;
    private String mittente;
    private String nomeFlusso;
    private String nRifTransazione;
    private String depositoSwift;
    private String rapporto;
    private String depositario;
    private String tipoDeposisto;
    private String depositoSf3;
    private String dtSaldo;       /*DATA della transazione*/
    private String segno;
    private String divisa;              /*divisa di saldo*/
    private String saldo;
    public String cambio;               /*tasso di cambio(1 o 0)*/
    public String ctvDivConto;          /*saldo convertito*/
    public String divConto;     /*divisa relativa alla residenza*/


}
