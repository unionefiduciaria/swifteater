package it.unionefiduciaria.swift.saldi.adapters.shellAdapter;


import com.prowidesoftware.swift.model.SwiftMessage;
import it.unionefiduciaria.swift.saldi.port.ISwiftMessageManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@ShellComponent
public class ShellMode {

    @Autowired
    ISwiftMessageManager swiftMessageManager;

    @ShellMethod(value = "inserire percorso", key = "setdir")
    public void addDir(@ShellOption(defaultValue = "src\\main\\Input") String directory) throws IOException {

        List<String> listOfNameFileMessage = new ArrayList<>();

        Stream<Path> path = Files.walk(Paths.get("src\\main\\Input"));
        path.forEach(s -> listOfNameFileMessage.add(s.getFileName().toString()));
        listOfNameFileMessage.remove(0);

        for (String nameFile : listOfNameFileMessage) {
            try {
                this.swiftMessageManager.readResource(nameFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

//        /*TODO: parallellizzazione del servizio SPERIMANTALE!*/
//        listOfNameFileMessage.parallelStream().forEach(s-> {
//            try {
//                this.swiftMessageManager.readResource(s);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        });


    }

}
