package it.unionefiduciaria.swift.saldi.adapters.api;

import it.unionefiduciaria.swift.saldi.model.ConvertedAmount;
import it.unionefiduciaria.swift.saldi.port.IRestExchangePort;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping
public class RestExchangeResource implements IRestExchangePort {

    RestTemplate restTemplate;


    public RestExchangeResource() {
    }


    public ConvertedAmount restModel(String divConto, String saldo, String divisa, String date) {

        ConvertedAmount convertedAmount = this.restTemplate.getForObject(
                "http://localhost:8080/convert-to?amount=" + saldo + "&currency_from=" + divConto + "&currency_to=" + divisa + "&date=" + date,
                ConvertedAmount.class);

        return convertedAmount;
    }

}
