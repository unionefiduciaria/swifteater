package it.unionefiduciaria.swift.saldi.adapters.logsManager;

import com.prowidesoftware.swift.model.SwiftMessage;
import com.prowidesoftware.swift.model.mt.mt9xx.*;
import it.unionefiduciaria.swift.saldi.model.ConvertedAmount;
import it.unionefiduciaria.swift.saldi.port.IExchange;
import it.unionefiduciaria.swift.saldi.port.ILogsResourcePort;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

@Component
public class LogsResource implements ILogsResourcePort {

    IExchange exchange;


    public String getExchangeValueFromComvertAmont(String divConto, String saldo, String divisa, String date) {


        ConvertedAmount convertedAmount = exchange.getConvertAmount(divConto, saldo, divisa, date);

        String cambio = null;

        if (divisa.equals(divConto)) {
            cambio = "1";

        } else {
            exchange.getConvertAmount(divConto, saldo, divisa, date);

            /*TODO:rettificare il controllo se la risorsa non viene trova tassi di cambio*/

            if (convertedAmount == null) {
                cambio = "0";

            } else {
                cambio = convertedAmount.getExchangeRates().get(0).getRate().toString();
            }
        }

        return cambio;
    }


    @Override
    public void addLog(SwiftMessage swiftMessage, String nameDir) {

        /*TODO:refactorare in modo che gestisca un valore per volta*/


//        RestResource restResource = new RestResource();


//        for (SwiftMessage swiftMessage : listSwiftMessage) {

        SaldoSwift et = new SaldoSwift();

            if (swiftMessage.getType().equals("940")) {


                MT940 mt = new MT940(swiftMessage);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");


                // String bic = msg2.getBlock1().getBIC().getBic8();
                et.setBic(swiftMessage.getBlock1().getBIC().getBic8());
                et.setMittente(mt.getSender());
                et.setMessaggio(mt.getMessageType());
                et.setNomeFlusso(nameDir);
                et.setNRifTransazione(mt.getField20().getValue());
                et.setDepositario(mt.getField25().getValue().replaceAll("/", ""));
                et.setRapporto("N.D");
                et.setDepositario("N.D");
                et.setTipoDeposisto("N.D");
                et.setDepositoSf3("N.D");
                et.setDtSaldo(sdf.format(mt.getField60F().getDateAsCalendar().getTime()));
                et.setSegno(mt.getField60F().getDCMark());
                et.setDivisa(mt.getField60F().getCurrency());
                et.setSaldo(mt.getField60F().getAmount());

                et.setDivConto(getExchangeValueFromComvertAmont(
                        "USD",
                        mt.getField60F().getAmount().replaceAll(",", "."),
                        /*mt.getField60F().getCurrency()*/"CHF",
                        /*sdf.format(mt.getField60F().getDateAsCalendar().getTime().toString())*/"2020-10-18"));


//                et.setCtvDivConto( restResource.getRestExchange(
//                        "USD",
//                        mt.getField60F().getAmount().replaceAll(",","."),
//                        /*mt.getField60F().getCurrency()*/"CHF",
//                        /*sdf.format(mt.getField60F().getDateAsCalendar().getTime().toString())*/"2020-10-18"));
//
//                et.setCambio(restResource.getConvertedAmount().getExchangeRates().get(0).getRate().toString());
//                et.setSaldo(restResource.getConvertedAmount().getAmount().toString());


            } else if (swiftMessage.getType().equals("941")) {

                MT941 mt = new MT941(swiftMessage);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");


                et.setBic(swiftMessage.getBlock1().getBIC().getBic8());
                et.setMittente(mt.getSender());
                et.setMessaggio(mt.getMessageType());
                et.setNomeFlusso(nameDir);
                et.setNRifTransazione(mt.getField20().getValue());
                et.setDepositoSwift(mt.getField25().getValue().replaceAll("/", ""));
                et.setRapporto("N.D");
                et.setDepositario("N.D");
                et.setTipoDeposisto("N.D");
                et.setDepositoSf3("N.D");
                et.setDtSaldo(sdf.format(mt.getField62F().getDateAsCalendar().getTime()));
                et.setSegno(mt.getField62F().getDCMark());
                et.setDivisa(mt.getField62F().getCurrency());
                et.setSaldo(mt.getField62F().getAmount());


            } else if (swiftMessage.getType().equals("950")) {

                MT950 mt = new MT950(swiftMessage);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

                et.setBic(swiftMessage.getBlock1().getBIC().getBic8());
                et.setMittente(mt.getSender());
                et.setMessaggio(mt.getMessageType());
                et.setNomeFlusso(nameDir);
                et.setNRifTransazione(mt.getField20().getValue());
                et.setDepositoSwift(mt.getField25().getValue().replaceAll("/", ""));
                et.setRapporto("N.D");
                et.setDepositario("N.D");
                et.setTipoDeposisto("N.D");
                et.setDepositoSf3("N.D");
                et.setDtSaldo(sdf.format(mt.getField62F().getDateAsCalendar().getTime()));
                et.setSegno(mt.getField62F().getDCMark());
                et.setDivisa(mt.getField62F().getCurrency());
                et.setSaldo(mt.getField62F().getAmount());



            }


//            logger.trace
            System.out.println(et.getBic()
                    + " " + et.getMittente()
                    + " " + et.getMessaggio()
                    + " " + et.getNomeFlusso()
                    + " " + et.getNRifTransazione()
                    + " " + et.getDepositoSwift()
                    + " " + et.getRapporto()
                    + " " + et.getDepositario()
                    + " " + et.getTipoDeposisto()
                    + " " + et.getDepositoSf3()
                    + " " + et.getDtSaldo()
                    + " " + et.getSegno()
                    + " " + et.getDivisa()
                    + " " + et.getSaldo()
                    + " " + et.getCtvDivConto());


//        }


    }

}
