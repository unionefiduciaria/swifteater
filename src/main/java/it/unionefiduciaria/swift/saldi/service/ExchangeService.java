package it.unionefiduciaria.swift.saldi.service;

import it.unionefiduciaria.swift.saldi.model.ConvertedAmount;
import it.unionefiduciaria.swift.saldi.port.IExchange;
import it.unionefiduciaria.swift.saldi.port.IRestExchangePort;

public class ExchangeService implements IExchange {


    private IRestExchangePort restExchangePort;


    @Override
    public ConvertedAmount getConvertAmount(String currencyFrom, String currencyTo, String amount, String date) {

        return this.restExchangePort.restModel(currencyFrom, currencyTo, amount, date);
    }
}
