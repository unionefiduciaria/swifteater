package it.unionefiduciaria.swift.saldi.service;

import java.io.IOException;
import java.util.List;

import it.unionefiduciaria.swift.saldi.port.ILocalResourcePort;
import it.unionefiduciaria.swift.saldi.port.ILogsResourcePort;
import it.unionefiduciaria.swift.saldi.port.ISwiftMessageManager;

import com.prowidesoftware.swift.model.SwiftMessage;


public class SwiftMessageManagerServiceImpl implements ISwiftMessageManager {

    private ILocalResourcePort localResourcePort;
    private ILogsResourcePort logsResourcePort;


    @Override
    public List<SwiftMessage> readResource(String fileName) throws IOException {

        /*TODO: controllo se la stringa serve per una cartella o è un'indirizzo su db */

        return localResourcePort.readLocalResource(fileName);

    }

    @Override
    public void StoreFile(List<SwiftMessage> swift) throws IOException {

    }

    @Override
    public void saveLog(List<SwiftMessage> swift, String nameDir) {

        for (SwiftMessage swiftMessage : swift) {

            this.logsResourcePort.addLog(swiftMessage, nameDir);
        }
    }


////    #region vecchia versione
//
//    public Logger logger = LogManager.getLogger(ReadSwiftMessageImpl.class);
//    private String dir;
//
//
//
//    public ReadSwiftMessageImpl() {
//    }
//
//
//    //    gli dò in pasto un percorso e mi restituisce tutti i mesaggi presenti
//    public List<SwiftMessage> readResource(String dir) throws IOException {
//
//        this.dir = dir;
//        AbstractMT msg = AbstractMT.parse(Lib.readResource(this.dir));
//        SwiftMessage swiftmessage = msg.getSwiftMessage();
//        return filterConfiguration(swiftmessage);
//
//    }
//
//
//    public List<SwiftMessage> filterConfiguration(SwiftMessage swiftmessage) {
//
//        List<SwiftMessage> listOfMessage = new ArrayList<>();
//
//        for (int i = 0; i < swiftmessage.getUnparsedTextsSize(); i++) {
//
//            SwiftMessage swiftmessageobject = swiftmessage.unparsedTextGetAsMessage(i);
//            SwiftMessage swiftMessageTemp = null;
//
////            TODO: rimuovere duplicati
//            swiftMessageTemp = Filter.filter(swiftmessageobject).ifType("950").notIfSender(Arrays.asList("VONTCHZZ", "BLFLCHBB", "BLFLLI2X")).toSwiftMessage();
//
//            if (swiftMessageTemp != null) {
//                listOfMessage.add(swiftMessageTemp);
//                continue;
//            }
//            swiftMessageTemp = Filter.filter(swiftmessageobject).ifType("941").notIfSender(Arrays.asList("BPPBCHGG", "CBLUCH22")).toSwiftMessage();
//
//            if (swiftMessageTemp != null) {
//                listOfMessage.add(swiftMessageTemp);
//                continue;
//            }
//            swiftMessageTemp = Filter.filter(swiftmessageobject).ifType("940").toSwiftMessage();
//
//            if (swiftMessageTemp != null) {
//                listOfMessage.add(swiftMessageTemp);
//            }
//        }
//        return listOfMessage;
//    }
//
////    #endregion

}
